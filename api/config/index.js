require('dotenv').config();

const config = {
    api: {
        port: process.env.PORT || 8080,
    },
    dev: process.env.NODE_ENV !== 'production',
    cors: process.env.CORS || '*',
    db: {
        URL: process.env.DB_URL,
        dbUser: process.env.DB_USER,
        dbPassword: process.env.DB_PASSWORD,
        dbHost: process.env.DB_HOST,
        dbName: process.env.DB_NAME
    },
    jwt: {
        secret: process.env.JWT_SECRET
    },
    sentry: {
        sentryDns: process.env.SENTRY_DNS,
        sentryId: process.env.SENTRY_ID
    }
};

module.exports = { config };
