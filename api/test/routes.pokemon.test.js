const proxyquire = require('proxyquire');
const { pokemonMock, PokemonControllerMock } = require('../utils/mocks/pokemon')
const testServer = require('../utils/testServer');

describe('routes - pokemon', function () {
    const route = proxyquire('../api/components/pokemon/network', {
        'controller': PokemonControllerMock
    });

    const request = testServer(route);
    describe('GET /pokemon', function () {
        it('should respond with status 200', function (done) {
            request.get('/').expect(200, done);
        });

        it('should respond with the list of pokemon', async (done) => {
            const response = await request.get('/')
            const expectResponse = {
                error: false,
                status: 200,
                body: pokemonMock.slice(0, 20)
            }
            expect(response.body).toEqual(expectResponse);
            expect(response.body.body.length).toEqual(20)
            done()
        });
    });
});