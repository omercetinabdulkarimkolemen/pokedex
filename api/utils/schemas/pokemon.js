const joi = require('@hapi/joi');

const pokemonIdSchema = joi.number();

module.exports = {
    pokemonIdSchema
}