const fs = require('fs');
const path = require('path')

const urlFile = path.resolve('../pokedex/api/app.tpl.yaml')
const template = fs.readFileSync(urlFile).toString();
const content = renderTemplate(template, process.env);

const urlFileWrite = path.resolve('../pokedex/api/app.yaml')
fs.writeFileSync(urlFileWrite, content);

function renderTemplate(template, variables) {
    Object
        .entries(variables)
        .forEach(([key, value]) => {
            template = template.replace(new RegExp(`<% ${key} %>`, 'g'), value);
        });
    return template;
}