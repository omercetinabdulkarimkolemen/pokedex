const express = require('express')
const router = express.Router();
const Controller = require('./controller')

const validationHandler = require('../../../utils/middleware/validationHandler');
const { pokemonIdSchema } = require('../../../utils/schemas/pokemon')
const response = require('../../../network/response')

router.get('/', list);
router.get('/:pokemonId', validationHandler({ pokemonId: pokemonIdSchema }, 'params'), get);

async function list(req, res, next) {
    try {
        let { page } = req.query;
        if (!page) {
            page = 1;
        }
        const pokemons = await Controller.listPokemon({ page });
        response.success(req, res, pokemons, 200);
    } catch (err) {
        next(err)
    }
}

async function get(req, res, next) {
    try {
        const { pokemonId } = req.params;
        const pokemon = await Controller.getPokemon({ pokemonId });
        response.success(req, res, pokemon, 200)
    } catch (err) {
        next(err)
    }
}


module.exports = router
