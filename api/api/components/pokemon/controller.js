const mocks = require('../../../utils/mocks/pokemon')

function listPokemon({ page }) {
    return mocks.listPokemon({ page });
}

function getPokemon({ pokemonId }) {
    return mocks.getPokemon({ pokemonId });
}

module.exports = {
    listPokemon,
    getPokemon
};